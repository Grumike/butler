# Appliance Control Rest API

Here goes the list of rest methods with examples of request and response data

By default application is running without context name, so methods path should go directly after the port (http://localhost:8080 apply 1.1 -> http://localhost:8080/devices)

## Constants

### Oven baking states: 
    BAKING, COOLING, COLD, WARMING
### Washing Machine process states:
    RUNNING, STOPPED, PAUSED
### Washing Machine Clothes:
    COLOR, WHITE, WOOLEN, SILK
### Washing class:
    A, B

## 1. Devices

1.1 Retrieve list of the devices

GET /devices

Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
[
      {
      "id": 1,
      "name": "WonderMat",
      "serial": "1997-VAC3000"
   },
      {
      "id": 2,
      "name": "Monster Cook",
      "serial": "STEAM-667"
   }
]

1.2 Add another device - not implemented

POST /devices

Response:
HTTP/1.1 500 
Content-Type: application/json;charset=UTF-8
Body:
Not implemented in this version

## 2. State view, state update

2.1 Get the state of device (id could be picked from 1.1)

GET /devices/{id}/state
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
{
   "device":    {
      "id": 2,
      "name": "Monster Cook",
      "serial": "STEAM-667"
   },
   "state":    {
      "id": 2,
      "online": true,
      "bakingState": "COLD",
      "temperature": 80
   }
}

2.2 Set updated set for the device (id from 1.1)

Currently two types of devices are supported, so peak the appropriate state (Oven - see 2.2 "state", WashingMashing - see this request)

PUT /devices/{id}/state
Content-Type: application/json;charset=UTF-8
Body:
{
      "id": 1,
      "online": true,
      "process": "STOPPED",
      "conditions":       {
         "temperature": 70,
         "rpm": 1000,
         "washClass": "A",
         "clothesType": "COLOR",
         "drying": true
      }
 }
*Note: state id and device id should be the same value
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
{
   "device":    {
      "id": 1,
      "name": "WonderMat",
      "serial": "1997-VAC3000"
   },
   "state":    {
      "id": 1,
      "online": true,
      "process": "STOPPED",
      "conditions":       {
         "temperature": 70,
         "rpm": 1000,
         "washClass": "A",
         "clothesType": "COLOR",
         "drying": true
      }
   }
}

## 3. State shortcuts - small operations on some state values

3.1 Check if device is on (id from 1.1)

GET /devices/{id}/state/on
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
true

3.2 Turn device on (id from 1.1)

PUT /devices/{id}/state/on
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
{
   "device":    {
      "id": 2,
      "name": "Monster Cook",
      "serial": "STEAM-667"
   },
   "state":    {
      "id": 2,
      "online": true,
      "bakingState": "COLD",
      "temperature": 80
   }
}

3.3 Turn device off (id from 1.1)

DELETE /devices/{id}/state/on
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:

   "device":    {
      "id": 2,
      "name": "Monster Cook",
      "serial": "STEAM-667"
   },
   "state":    {
      "id": 2,
      "online": false,
      "bakingState": "COLD",
      "temperature": 80
   }
}

3.4 Get the temperature settings (id from 1.1)

GET /devices/{id}/state/temperature
Response:
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
80.0

3.5 Set the temperature value (id from 1.1, value is of float type)

PUT /devices/{id}/state/temperature/{value}
HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Body:
{
   "device":    {
      "id": 2,
      "name": "Monster Cook",
      "serial": "STEAM-667"
   },
   "state":    {
      "id": 2,
      "online": false,
      "bakingState": "COLD",
      "temperature": 90
   }
}