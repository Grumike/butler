package com.electro.butler;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.electro.butler.dao.DeviceRepository;
import com.electro.butler.dao.model.Conditions;
import com.electro.butler.dao.model.Conditions.ClothesType;
import com.electro.butler.dao.model.Conditions.WashClass;
import com.electro.butler.dao.model.Device;
import com.electro.butler.dao.model.DeviceType;
import com.electro.butler.dao.model.WashingMachineState;
import com.electro.butler.dao.model.WashingMachineState.ProcesState;
import com.electro.butler.rest.dto.DeviceDto;
import com.electro.butler.rest.dto.DeviceStateDto;
import com.electro.butler.service.DeviceService;

@RunWith(SpringRunner.class)
public class ButlerApplicationTests {
    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {
  
        @Bean
        public DeviceService deviceService() {
            return new DeviceService();
        }
    }
    
    @Autowired
    private DeviceService deviceService;
    
    @MockBean
    private DeviceRepository deviceRepository;
    
    
    @Before
    public void setUp() {
        Device device = new Device();
        device.setId(1l);
        device.setName("WonderMat");
        device.setSerial("1997-VAC3000");
        device.setDeviceType(DeviceType.WASHING_MACHINE);

        WashingMachineState state = new WashingMachineState();
        device.setState(state);

        Conditions conditions = new Conditions();

        state.setDevice(device);
        state.setConditions(conditions);
        state.setOnline(true);
        state.setProcess(ProcesState.STOPPED);

        conditions.setClothesType(ClothesType.WHITE);
        conditions.setTemperature(60f);
        conditions.setWashClass(WashClass.B);
        conditions.setRpm(1000);
        conditions.setDrying(false);
        
        Mockito.when(deviceRepository.findById(device.getId())).thenReturn(Optional.of(device));
        Mockito.when(deviceRepository.findAll()).thenReturn(Collections.singletonList(device));
    }
    
    @Test(expected = UnsupportedOperationException.class)
    public void testAddNewDeviceUnsupported() {
        deviceService.addDevice(null);
    }

    @Test
    public void testGetDevices() {
        List<DeviceDto> devices = deviceService.getAvailableDevices();
        assertEquals(1, devices.size());
    }

    @Test
    public void testGetState() {
        DeviceStateDto stateDto = deviceService.getState(1l);
        assertEquals(ProcesState.STOPPED, ((WashingMachineState) stateDto.getState()).getProcess());
    }
    
    @Test
    public void testTurnOff() {
        DeviceStateDto stateDto = deviceService.getState(1l);
        WashingMachineState state = (WashingMachineState) stateDto.getState();
        assertEquals(true, state.getOnline());
        deviceService.turnOnOff(1L, false);
        stateDto = deviceService.getState(1l);
        assertEquals(false, state.getOnline());
    }
    
    // TODO - Add more significant unit tests!
}
