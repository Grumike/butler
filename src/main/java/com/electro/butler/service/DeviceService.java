package com.electro.butler.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.electro.butler.dao.DeviceRepository;
import com.electro.butler.dao.model.Conditions;
import com.electro.butler.dao.model.Conditions.ClothesType;
import com.electro.butler.dao.model.Conditions.WashClass;
import com.electro.butler.dao.model.Device;
import com.electro.butler.dao.model.DeviceState;
import com.electro.butler.dao.model.DeviceType;
import com.electro.butler.dao.model.OvenState;
import com.electro.butler.dao.model.OvenState.BakingState;
import com.electro.butler.dao.model.WashingMachineState;
import com.electro.butler.dao.model.WashingMachineState.ProcesState;
import com.electro.butler.rest.dto.DeviceDto;
import com.electro.butler.rest.dto.DeviceStateDto;
import com.electro.butler.service.utils.DtoConverters;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Business logic for device management
 * 
 * @author mike
 *
 */
@Service
public class DeviceService {

    private final static Logger log = LoggerFactory.getLogger(DeviceService.class);

    @Autowired
    private DeviceRepository deviceRepository;

    /**
     * Initialization of the data tables if empty
     */
    @PostConstruct
    @Transactional
    public void init() {
        if (deviceRepository.countRecords() < 1) {
            Device device = new Device();
            device.setName("WonderMat");
            device.setSerial("1997-VAC3000");
            device.setDeviceType(DeviceType.WASHING_MACHINE);

            WashingMachineState state = new WashingMachineState();
            device.setState(state);

            Conditions conditions = new Conditions();

            state.setDevice(device);
            state.setConditions(conditions);
            state.setOnline(true);
            state.setProcess(ProcesState.STOPPED);

            conditions.setClothesType(ClothesType.WHITE);
            conditions.setTemperature(60f);
            conditions.setWashClass(WashClass.B);
            conditions.setRpm(1000);
            conditions.setDrying(false);

            deviceRepository.save(device);

            device = new Device();
            device.setName("Monster Cook");
            device.setSerial("STEAM-667");
            device.setDeviceType(DeviceType.OVEN);
            OvenState ovenState = new OvenState();
            device.setState(ovenState);
            ovenState.setDevice(device);
            ovenState.setBakingState(BakingState.COLD);
            ovenState.setTemperature(80f);
            ovenState.setOnline(true);
            deviceRepository.save(device);
        }
    }

    /**
     * Get list of available devices
     * 
     * @return List of devices
     */
    @Transactional
    public List<DeviceDto> getAvailableDevices() {
        List<Device> devices = deviceRepository.findAll();
        return devices.stream().map(e -> {return DtoConverters.fromDevice(e);}).collect(Collectors.toList());
    }

    /**
     * Add new device to the system
     * @param device
     * @return information about new device
     */
    @Transactional
    public DeviceDto addDevice(DeviceDto device) {
        throw new UnsupportedOperationException("Not implemented in this version");
    }

    /**
     * Retrieve information on the device state
     * @param id
     * @return DTO object with full device information
     */
    @Transactional
    public DeviceStateDto getState(Long id) {
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            return DtoConverters.fromDeviceToState(searchResult.get());
        }
        return null;
    }

    /**
     * Check if device is online
     * @param id of the device
     * @return true if online - false otherwise
     */
    @Transactional
    public Boolean check(Long id) {
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            return searchResult.get().getState().getOnline();
        }
        return false;
    }

    /**
     * Turn device on and off
     * @param id of device
     * @param on - true or false
     * @return state of the device
     */
    @Transactional
    public DeviceStateDto turnOnOff(Long id, boolean on) {
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            Device device = searchResult.get();
            device.getState().setOnline(on);
            deviceRepository.save(device);
            return DtoConverters.fromDeviceToState(device);
        }
        return null;
    }

    /**
     * Get temperature settings for the device
     * @param id of the device
     * @return temperature settings value
     */
    @Transactional
    public Float getTemperature(Long id) {
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            Device device = searchResult.get();
            Float result = null;
            switch(device.getDeviceType()) {
            case OVEN:
                OvenState ovenState = (OvenState) device.getState(); 
                result = ovenState.getTemperature();
                break;
            case WASHING_MACHINE:
                WashingMachineState washingMachineState = (WashingMachineState) device.getState();
                result = washingMachineState.getConditions().getTemperature();
                break;
            }
            
            return result;
        }
        return null;
    }
    
    /**
     * Set temperature value for the device settings
     * @param id of the device
     * @param value - temperature to set
     * @return State of the device
     */
    @Transactional
    public DeviceStateDto setTemperature(Long id, Float value) {
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            Device device = searchResult.get();
            switch(device.getDeviceType()) {
            case OVEN:
                OvenState ovenState = (OvenState) device.getState(); 
                ovenState.setTemperature(value);
                break;
            case WASHING_MACHINE:
                WashingMachineState washingMachineState = (WashingMachineState) device.getState();
                washingMachineState.getConditions().setTemperature(value);
                break;
            }
            deviceRepository.save(device);
            return DtoConverters.fromDeviceToState(device);
        }
        return null;
    }

    /**
     * Allows to set all values to the state of device in one operation
     * @param id of the device
     * @param body - string containing JSON for the new device state
     * @return New state of the device
     */
    @Transactional
    public DeviceStateDto setState(Long id, String body) {
        if(log.isDebugEnabled()) {
            log.debug(body);
        }
        Optional<Device> searchResult = deviceRepository.findById(id);
        if(searchResult.isPresent()) {
            Device device = searchResult.get();
            ObjectMapper mapper = new ObjectMapper();
            try {
                switch(device.getDeviceType()) {
                case OVEN:
                    OvenState ovenState = mapper.readValue(body, OvenState.class);
                    checkId(device.getId(), ovenState);
                    DtoConverters.copyOvenState(device, ovenState);
                    break;
                case WASHING_MACHINE:
                    WashingMachineState washingMachineState = mapper.readValue(body, WashingMachineState.class);
                    checkId(device.getId(), washingMachineState);
                    DtoConverters.copyWashingMachineState(device, washingMachineState);
                    break;
                }
                deviceRepository.save(device);
                return DtoConverters.fromDeviceToState(device);
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new RuntimeException(e.getMessage(), e);
            }
        }
        return null;
    }

    private void checkId(Long id, DeviceState state) {
        if(!id.equals(state.getId())) {
            throw new IllegalArgumentException("Invalid device or device state id");
        }
    }
}
