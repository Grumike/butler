package com.electro.butler.service.utils;

import com.electro.butler.dao.model.Conditions;
import com.electro.butler.dao.model.Device;
import com.electro.butler.dao.model.OvenState;
import com.electro.butler.dao.model.WashingMachineState;
import com.electro.butler.rest.dto.DeviceDto;
import com.electro.butler.rest.dto.DeviceStateDto;

/**
 * Utility class for managing DTO - Entity conversion and other stuff
 * @author mike
 *
 */
public class DtoConverters {
    public static DeviceDto fromDevice(Device device) {
        return new DeviceDto(device.getId(), device.getName(), device.getSerial());
    }

    public static DeviceStateDto fromDeviceToState(Device device) {
        return new DeviceStateDto(fromDevice(device), device.getState());
    }

    public static void copyOvenState(Device device, OvenState ovenState) {
        OvenState state = (OvenState) device.getState();
        state.setBakingState(ovenState.getBakingState());
        state.setOnline(ovenState.getOnline());
        state.setTemperature(ovenState.getTemperature());
    }

    public static void copyWashingMachineState(Device device, WashingMachineState newState) {
        WashingMachineState deviceState = (WashingMachineState) device.getState();
        deviceState.setOnline(newState.getOnline());
        deviceState.setProcess(newState.getProcess());
        Conditions deviceConditions = deviceState.getConditions();
        Conditions newConditions = newState.getConditions();
        deviceConditions.setClothesType(newConditions.getClothesType());
        deviceConditions.setDrying(newConditions.getDrying());
        deviceConditions.setRpm(newConditions.getRpm());
        deviceConditions.setTemperature(newConditions.getTemperature());
        deviceConditions.setWashClass(newConditions.getWashClass());
    }
}
