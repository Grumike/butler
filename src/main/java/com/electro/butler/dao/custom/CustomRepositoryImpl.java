package com.electro.butler.dao.custom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * Implementation of JPA repository customization
 * @author mike
 *
 */
@Component
public class CustomRepositoryImpl implements CustomRepository {
    
    private static final String COUNT = "select count(*) from DEVICE";
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Integer countRecords() {
        return jdbcTemplate.queryForObject(COUNT, Integer.class);
    }

}
