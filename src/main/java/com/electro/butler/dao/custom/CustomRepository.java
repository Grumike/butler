package com.electro.butler.dao.custom;

/**
 * Means to customize JPA repository
 * @author mike
 *
 */
public interface CustomRepository {
    /**
     * Count records in device table
     * @return count of rows
     */
    public Integer countRecords();
}
