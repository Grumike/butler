package com.electro.butler.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Special properties of the OVEN devices
 * @author mike
 *
 */
@Entity
public class OvenState extends DeviceState {
    public static enum BakingState {
        BAKING, COOLING, COLD, WARMING
    };

    @Column
    @Enumerated(EnumType.STRING)
    private BakingState bakingState;
    
    @Column
    private Float temperature;

    public BakingState getBakingState() {
        return bakingState;
    }

    public void setBakingState(BakingState bakingState) {
        this.bakingState = bakingState;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }
}
