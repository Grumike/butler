package com.electro.butler.dao.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Portion of washing machine state
 * @author mike
 *
 */
@Embeddable
public class Conditions {
    
    public static enum WashClass{A, B};
    public static enum ClothesType{COLOR, WHITE, WOOLEN, SILK};
    
    @Column
    private Float temperature;
    
    @Column
    private Integer rpm;
    
    @Column
    @Enumerated(EnumType.STRING)
    private WashClass washClass;

    @Column
    @Enumerated(EnumType.STRING)
    private ClothesType clothesType;
    
    @Column
    private Boolean drying;

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Integer getRpm() {
        return rpm;
    }

    public void setRpm(Integer rpm) {
        this.rpm = rpm;
    }

    public WashClass getWashClass() {
        return washClass;
    }

    public void setWashClass(WashClass washClass) {
        this.washClass = washClass;
    }

    public ClothesType getClothesType() {
        return clothesType;
    }

    public void setClothesType(ClothesType clothesType) {
        this.clothesType = clothesType;
    }

    public Boolean getDrying() {
        return drying;
    }

    public void setDrying(Boolean drying) {
        this.drying = drying;
    }

}
