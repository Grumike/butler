package com.electro.butler.dao.model;

/**
 * Devices supported by application
 * @author mike
 *
 */
public enum DeviceType {
    WASHING_MACHINE, OVEN
}
