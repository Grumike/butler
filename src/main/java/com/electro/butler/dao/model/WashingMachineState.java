package com.electro.butler.dao.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * Special properties of washing machines
 * @author mike
 *
 */
@Entity
public class WashingMachineState extends DeviceState {
    
    public static enum ProcesState {RUNNING, STOPPED, PAUSED};
    
    @Column
    @Enumerated(EnumType.STRING)
    private ProcesState process;
    
    @Embedded
    private Conditions conditions;

    public ProcesState getProcess() {
        return process;
    }

    public void setProcess(ProcesState process) {
        this.process = process;
    }

    public Conditions getConditions() {
        return conditions;
    }

    public void setConditions(Conditions conditions) {
        this.conditions = conditions;
    }
}
