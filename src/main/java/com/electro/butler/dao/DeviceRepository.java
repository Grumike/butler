package com.electro.butler.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.electro.butler.dao.custom.CustomRepository;
import com.electro.butler.dao.model.Device;

/**
 * Customized JPA repository - DAO operations support
 * @author mike
 *
 */
public interface DeviceRepository  extends CrudRepository<Device, Long>, CustomRepository {
    /**
     * Get all available devices
     */
    List<Device> findAll();
    /**
     * Get device by its id
     */
    Optional<Device> findById(Long id);
}
