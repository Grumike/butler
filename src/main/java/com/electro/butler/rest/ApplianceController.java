package com.electro.butler.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.electro.butler.rest.dto.DeviceDto;
import com.electro.butler.rest.dto.DeviceStateDto;
import com.electro.butler.service.DeviceService;

/**
 * API Facade interface for appliance control
 * 
 * @author mike
 *
 */
@RestController
@RequestMapping("/devices")
public class ApplianceController {

    private final static Logger log = LoggerFactory.getLogger(ApplianceController.class);
    
    @ExceptionHandler
    ResponseEntity<String> handle(Exception ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<String>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
    public List<DeviceDto> listDevices() {
        return deviceService.getAvailableDevices();
    }

    @RequestMapping(value = "/{id}/state", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
    public DeviceStateDto getState(@PathVariable Long id) {
        return deviceService.getState(id);
    }

    @RequestMapping(value = "/{id}/state", consumes = "application/json; charset=UTF-8", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
    public DeviceStateDto setState(@RequestBody String body, @PathVariable Long id) {
        return deviceService.setState(id, body);
    }

    @RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
    public DeviceDto createDevice() {
        return deviceService.addDevice(new DeviceDto());
    }
    
    @RequestMapping(value = "/{id}/state/on", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
    public Boolean check(@PathVariable Long id) {
        return deviceService.check(id);
    }

    @RequestMapping(value = "/{id}/state/on", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
    public DeviceStateDto turnOff(@PathVariable Long id) {
        return deviceService.turnOnOff(id, false);
    }

    @RequestMapping(value = "/{id}/state/on", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
    public DeviceStateDto turnOn(@PathVariable Long id) {
        return deviceService.turnOnOff(id, true);
    }

    @RequestMapping(value = "/{id}/state/temperature", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
    public Float getTemperature(@PathVariable Long id) {
        return deviceService.getTemperature(id);
    }

    @RequestMapping(value = "/{id}/state/temperature/{value}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
    public DeviceStateDto setTemperature(@PathVariable Long id, @PathVariable Float value) {
        return deviceService.setTemperature(id, value);
    }
}
