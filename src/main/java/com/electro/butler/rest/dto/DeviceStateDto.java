package com.electro.butler.rest.dto;

import com.electro.butler.dao.model.DeviceState;

/**
 * Full information on device and its state for pushing to the REST client
 * @author mike
 *
 */
public class DeviceStateDto {
    
    private DeviceDto device;
    
    private DeviceState state;

    public DeviceStateDto() {
    }

    public DeviceStateDto(DeviceDto device, DeviceState state) {
        this.device = device;
        this.state = state;
    }

    public DeviceDto getDevice() {
        return device;
    }

    public void setDevice(DeviceDto device) {
        this.device = device;
    }

    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

}
