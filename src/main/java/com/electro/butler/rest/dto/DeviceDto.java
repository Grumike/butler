package com.electro.butler.rest.dto;

/**
 * Information on device for pushing to the REST client
 * 
 * @author mike
 *
 */
public class DeviceDto {

    private Long id;
    private String name;
    private String serial;

    public DeviceDto() {
    }

    public DeviceDto(Long id, String name, String serial) {
        this.id = id;
        this.name = name;
        this.serial = serial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }
}
