package com.electro.butler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application entrance point, executable class
 * @author mike
 *
 */
@SpringBootApplication
public class ButlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ButlerApplication.class, args);
    }
}
