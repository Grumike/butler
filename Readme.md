# Backend developer test
# Appliance Control

## Description

This is a demo Spring Boot application, which represents a toy RESTfull API suit
for managing some imagined park of home devices. The couple of devices are generated
on the first start of application and are backed up by h2 file database (resides in 
etc/db/ folder) named elx. Application does not provide the ability to add new devices,
but it could be easily done by means of SQL through the H2 console.
See http://www.h2database.com

REST API is documented in etc/doc/rest-api-doc.md

Also for convenience SoapUI project is provided here: etc/soapui/ELX-soapui-project.xml
See https://www.soapui.org/

## Build

Project "Butler" has to be built with maven directly from the root of the project:

> mvn clean install

## Run

There are variants:

1) Application could be run from the root of the project with maven like that:

> mvn spring-boot:run

2) Or alternatively you could start it with java from the root of the project:

> java -jar target/butler-0.0.1-SNAPSHOT.jar

These commands (whatever you choose) will start "Butler" REST services on port 8080

3) If this port is occupied you can change it like that:

java -jar target/butler-0.0.1-SNAPSHOT.jar --server.port=8081

Or

mvn spring-boot:run -Dspring-boot.run.jvmArguments='-Dserver.port=8083'

## Versions

For development and testing were used tools with listed versions:

java version 1.8.0_171
SoapUI version 5.4.0
maven version 3.0.4
Spring Boot 2.0.2.RELEASE
